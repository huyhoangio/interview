function twoSum(arr, target) {
  results = []
  diff_obj = {} // To store difference and index
  arr.forEach((num, index) => {
    if (num in diff_obj) {
      results.push({'index': diff_obj[num], 'value': target - num})
      results.push({'index': index, 'value': num})
      delete diff_obj[num]
      return;
    }
    
    diff = target - num;
    if (!(diff in diff_obj)) {
      diff_obj[diff] = index;
    } 
    
  })
  return results;
}

a = [3, 4, 9, 1, 7, 5, 5]
target_num = 15

console.log(twoSum(a, target_num))