function object_equals(o1, o2) {
  if (o1.length !== o2.length) {
    return false
  }
  for (const char in o1) {
    if (o1[char] !== o2[char]){
      return false
    }
  }
  return true
}

function word_to_obj(word) {
  obj = {}
  word = word.replace(/\s/g, '')
  for (var i = 0; i < word.length; i++) {
    char = word.charAt(i)
    if (!(char in obj)) {
      obj[char] = 0
    }
    obj[char]++
  }  
  return obj
}

function checkAnagram(word1, word2) {
  var first_word_dict = {}
  var second_word_dict = {}

  first_word_dict = word_to_obj(word1)
  second_word_dict = word_to_obj(word2)
  
  return object_equals(first_word_dict, second_word_dict)
  
}

word1 = "debit card"
word2 = "bad credit"

console.log(checkAnagram(word1, word2))